<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/light.css">
    <link rel="stylesheet" href="/css/style.css">
    <meta name="theme-color" content="#ffffff">
    <script>
    loadMode();
        
    function loadMode(){
        const darkMode = localStorage.getItem('dark');
        const prefersDarkMode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;

        if(darkMode == "yes"){
           document.querySelector('meta[name="theme-color"]').setAttribute("content", "#1E1E1E");
           return changeCSS("css/dark.css");
        }

        if (prefersDarkMode && darkMode != "no") {
            localStorage.setItem('dark', 'yes');
            return changeCSS("css/dark.css");
        } else {
            localStorage.setItem('dark', 'no');
            return changeCSS("css/light.css");
        }
    }

    function changeCSS(cssFile, cssLinkIndex = 0) {
        let oldlink = document.getElementsByTagName("link").item(cssLinkIndex);
        let newlink = document.createElement("link");
        newlink.setAttribute("rel", "stylesheet");
        newlink.setAttribute("type", "text/css");
        newlink.setAttribute("href", cssFile);
        document.getElementsByTagName("head").item(cssLinkIndex).replaceChild(newlink, oldlink);
    }
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Five Moods</title>
    <link rel="shortcut icon" href="logo.svg" type="image/svg">
    <link rel="stylesheet" href="https://cdn.steinke.dev/materialize.min.css">
    <link rel="stylesheet" href="fontawesome/css/all.min.css">
    <link rel="apple-touch-icon" sizes="180x180" href="https://fivemoods.ch/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://fivemoods.ch/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="194x194" href="https://fivemoods.ch/img/favicon-194x194.png">
    <link rel="icon" type="image/png" sizes="192x192" href="https://fivemoods.ch/img/android-chrome-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://fivemoods.ch/img/favicon-16x16.png">
    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/jklgfclibdmlbgglpmjbifkkgkgcchie">
    <link rel="manifest" href="https://fivemoods.ch/site.webmanifest">
    <link rel="mask-icon" href="https://fivemoods.ch/img/safari-pinned-tab.svg" color="#6ab02c">
    <meta name="apple-mobile-web-app-title" content="Five Moods">
    <meta name="application-name" content="Five Moods">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="https://fivemoods.ch/img/mstile-144x144.png">
    <meta name="title" content="Five Moods">
    <meta name="description" content="Einfache, simple Darstellung vom Five Moods Restaurant Menu in Zug.">
    <meta name="keywords" content="Five Moods, Restaurant Siemens, Zug, Siemens Schweiz AG, Personalrestaurant, SV (Schweiz) AG, SV Schweiz, SV, SV Business, Betriebsverpflegung, Mitarbeiterverpflegung, Personalgastronomie, Catering, Verpflegung, gesunde Verpflegung, Reservation, Bestellung, Angebot, Preise, SV Restaurants, Personalrestaurant, Personalrestaurants, Gaststätte, Kantine, Getränke, Oeffnungszeiten, Betriebsrestaurant, Bankett, Partyservice, Kaffee, Gerichte, preiswert, Menukarte, Fleisch, Fisch, Gemuese, Speisekarte, Sitzplätze, Take-away, Nichtraucher, Service, Self-service, self service, Selbstbedienung, Mittagessen, Fruehstueck, Essen, Gaeste, Menu, Tagesteller, Restaurant, Restaurants, vegetarisch, kreativ, guenstig, Spezialitaeten, 5 moods">
    <meta name="googlebot" content="index, follow">
    <meta name="google" content="notranslate">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://fivemoods.ch/">
    <meta property="og:title" content="Five Moods">
    <meta property="og:description" content="Einfache, simple Darstellung vom Five Moods Restaurant Menu in Zug.">
    <meta property="og:image" content="https://fivemoods.ch/img/android-chrome-192x192.png">
    <meta property="og:image:width" content="64">
    <meta property="og:image:height" content="64">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://fivemoods.ch/">
    <meta property="twitter:title" content="Five Moods">
    <meta property="twitter:description" content="Einfache, simple Darstellung vom Five Moods Restaurant Menu in Zug.">
    <meta property="twitter:image" content="https://fivemoods.ch/img/android-chrome-192x192.png">
</head>

<body>
    <nav>
        <div class="nav-wrapper fivemoods-nav">
        <a href="/" class="brand-logo center" id="navlogobar">
            <img src="logo.svg" alt="Five Moods Logo" id="navimage">
            <span id="navtitle">&nbsp;Five Moods</span>
        </a>

        <ul id="nav-mobile" class="right">
            <li>
                <a href="settings">
                    <i class="fas fa-cog"></i>
                </a>
            </li>
        </ul>
        </div>
    </nav>
    <div class="container">
        <div id="loader">
            <div id="loader-spinner"></div>
        </div>
        <div id="output" style="display: none;">
            <!-- Menu gets filled here by JS -->
        </div>
    </div>
    <div style="clear:both;"></div>
    <br>
    <!-- Footer -->
    <footer id="footer" style="display: none;" class="page-footer">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Five Moods</h5>
                <p class="grey-text text-lighten-4">
                    Diese Website ist in keiner Weise mit SV Groups verbunden.
                    Diese Website wird nicht von SV unterstützt.
                    Bei Fragen oder Anregungen senden Sie bitte eine E-Mail an <a class="grey-text text-lighten-3" href="mailto:support@fivemoods.ch?subject=Website%20Vorschlag&body=Information%3A%20Diese%20E-Mail%20ist%20NUR%20f%C3%BCr%20die%20Website%20FIVEMOODS.CH%20zust%C3%A4ndig%20-%20Wenn%20sie%20das%20Restaurant%20kontaktieren%20M%C3%B6chten%20benutzen%20sie%20bitte%20fivemoods%40sv-group.ch%0D%0A%0D%0ADIESE%20EMAIL%20IST%20NUR%20F%C3%9CR%20DIE%20WEBSITE%20ZUST%C3%84NDIG!!!%0D%0A%0D%0ABEI%20RESERVATIONEN%20ETC.%20BITTE%20WENDEN%20SIE%20SICH%20AN%20fivemoods%40sv-group.ch%0D%0A%0D%0AIhre%20E-Mail%20wird%20ansonsten%20ignoriert." title="Email">support@fivemoods.ch</a>
                    <br>
                    <br>
                    <button class="btn" onclick="addExtenstion()" id="install-button">Add to Chrome</button>
                    <script>
                        if (isFireFox()) {
                            document.getElementById('install-button').innerHTML = "Add to Firefox";
                        }

                        function addExtenstion() {
                            if (isFireFox()) {
                                window.open("https://addons.mozilla.org/en-US/firefox/addon/five-moods-restaurant/", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=700,height=500");
                            } else {
                                window.open("https://chrome.google.com/webstore/detail/five-moods-restaurant/jklgfclibdmlbgglpmjbifkkgkgcchie", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=700,height=500");
                            }
                        }

                        function isFireFox() {
                            return navigator.userAgent.search("Firefox") > -1;
                        }
                    </script>
                </p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                    <li><a class="grey-text text-lighten-3" href="settings">Einstellungen</a></li>
                    <li><a class="grey-text text-lighten-3" href="oeffnungszeiten">Öffnungszeiten</a></li>
                    <li><a rel="noopener noreferrer" class="grey-text text-lighten-3" href="https://www.buymeacoffee.com/kaisteinke" title="Donate" target="_blank">Spenden</a></li>
                    <li><a rel="noopener noreferrer" class="grey-text text-lighten-3" href="https://siemens.sv-restaurant.ch/de/menuplan/five-moods/" target="_blank">Offizielle Seite</a></li>
                    <li><a class="grey-text text-lighten-3" target="_blank" href="https://docs.fivemoods.ch">API Dokumentation</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            &copy; <?php echo date("Y"); ?> <a href="mailto:kai@steinke.dev" id="footer-copyright-text" target="_blank">Kai Steinke</a>
        </div>
    </div>
    </footer>
    <script defer src="https://cdn.steinke.dev/materialize.min.js"></script>
    <script defer src="/js/main.js"></script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-647X2MP41P"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-647X2MP41P');
    </script>
</body>

</html>