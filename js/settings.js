const checkbox = document.getElementById('siemensrabatt');
const checkbox2 = document.getElementById('darkmode');
const checkbox3 = document.getElementById('vegetarian');
const checkbox4 = document.getElementById('notvegi');

// Siemens Discount
checkbox.addEventListener('click', () => {
    (checkbox.checked) ? localStorage.setItem("siemens", "yes") : localStorage.removeItem("siemens");
    M.toast({ html: 'Einstellungen gespeichert!' });
});

// Dark Mode
checkbox2.addEventListener('click', () => {
    (checkbox2.checked) ? localStorage.setItem("dark", "yes") : localStorage.setItem("dark", "no");
    M.toast({ html: 'Einstellungen gespeichert!' });
    loadMode();
});

// vegetarian
checkbox3.addEventListener('click', () => {
    if (checkbox4.checked && checkbox3.checked) {
        checkbox4.checked = false;
        localStorage.removeItem("meat");
    }
    (checkbox3.checked) ? localStorage.setItem("vegetarian", "yes") : localStorage.removeItem("vegetarian");
    M.toast({ html: 'Einstellungen gespeichert!' });
});

// not vegetarian
checkbox4.addEventListener('click', () => {
    if (checkbox3.checked && checkbox4.checked) {
        checkbox3.checked = false;
        localStorage.removeItem("vegetarian");
    }
    (checkbox4.checked) ? localStorage.setItem("meat", "yes") : localStorage.removeItem("meat");
    M.toast({ html: 'Einstellungen gespeichert!' });
});

window.onload = () => {
    if (localStorage.getItem('siemens') !== null) {
        checkbox.checked = true;
    }
    if (localStorage.getItem('dark') === "yes") {
        checkbox2.checked = true;
    }
    if (localStorage.getItem('vegetarian') !== null) {
        checkbox3.checked = true;
    }
    if (localStorage.getItem('meat') !== null) {
        checkbox4.checked = true;
    }
}
