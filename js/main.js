const spinner = document.getElementById("loader");
const output = document.getElementById("output");
const footer = document.getElementById("footer");
const menu_url = "https://api-prod.fivemoods.ch/";

window.onload = async () => {
  // try and load menu
  await loadMenu();
  // hide spinner
  spinner.style.display = "none";
  // show menu
  output.style.display = "block";
  // show footer
  footer.style.display = "block";
  // init tooltips
  M.Tooltip.init(document.querySelectorAll('.tooltipped'), {
    exitDelay: 200,
  });
}

async function loadMenu() {
  // load menu
  await fetch(menu_url)
    .then(response => response.json())
    .then(data => {
      // create menu
      createMenu(data);
    })
    .catch(error => {
      // log error
      console.log(error);
      output.innerHTML = `
      <h5>Keine Menus verfügbar</h5>
      <p>Es scheint, als ob keine Menus verfügbar sind. Bitte versuche es später noch einmal.</p>`;
    });
}

function createMenu(data) {
  let weekday = "";
  let menusHTML = "";
  // loop through each menu item
  for (let i = 0; i < data.length; i++) {
    // if user is vegetarian and menu is not, skip
    if (localStorage.getItem("vegetarian") == "yes"){
      if(data[i].vegetarian == false){
        continue;
      }
    }
    // if user is only meat eater and menu is not, skip
    if (localStorage.getItem("meat") == "yes"){
      if(data[i].vegetarian == true){
        continue;
      }
    }
    // if there is a new weekday or first day, show weekday title
    if (weekday != data[i].germanweekday || i == 0) {
      weekday = data[i].germanweekday;
      menusHTML += `
      <div style="clear:both;"></div>
      <div>
        <h3>${weekday}</h3>
      </div>`;
    }
    // create menu card
    menusHTML += `<div class="col s6">`;
    if(data[i].vegetarian == true){
      menusHTML += `<div class="card vegi-card">`;
    } else {
      menusHTML += `<div class="card">`;
    }
    menusHTML += `<div class="card-content">`;
    
    // category ----------------------------------------
    menusHTML += `<p>`;
    switch (data[i].category) {
      case "Chef's Choice":
      case "Chefs Choice":
      case "Chef Choice":
        menusHTML += `<i class="far fa-hat-chef"></i>&nbsp;${data[i].category}`;
        break;
      case "Daily's":
      case "Daily's I":
      case "Daily's II":
        menusHTML += `<i class="far fa-calendar-day"></i>&nbsp;${data[i].category}`;
        break;
      case "Season Market":
        menusHTML += `<i class="fad fa-store"></i>&nbsp;${data[i].category}`;
      case "Go 4 Pasta / Pizza":
      case "Pasta / Pizza":
      case "Go 3 Pasta / Pizza":
        menusHTML += `<i class="far fa-pizza-slice"></i>&nbsp;${data[i].category}`;
        break;
      case "Hot & Cold":
        menusHTML += `<i class="fad fa-heat"></i>&nbsp;${data[i].category}`;
        break;
      case "":
        menusHTML += ``;
        break;
      default:
        menusHTML += `<i class="fas fa-utensils"></i>&nbsp;${data[i].category}`;
        break;
    }
    menusHTML += `</p>`;
    
    // price ----------------------------------------
    const price = data[i].price;
    const currency = data[i].currency;
    const siemens = localStorage.getItem("siemens") === "yes";
    const priceHTML = price && price !== 0 ? (siemens ? data[i].siemensprice : price) + " " + currency : "";
    menusHTML += `<span class="price">${priceHTML}</span>`;
    
    // menu title ----------------------------------------
    menusHTML += `<h4>${data[i].title}</h4>`;

    // description ----------------------------------------
    menusHTML += `<div class="card-action"></div>`;
    menusHTML += `<p class="ds">${data[i].description}`;
    if(data[i].allergens || data[i].nutrients.length > 0){
      menusHTML += `
      <br><a style="cursor:pointer" class="more-info-btn" onclick="showInfo('info-${i}', this);"><i class="fas fa-info-circle"></i>&nbsp;Mehr Informationen&nbsp;<i class="fal fa-angle-down"></i></a>
      `;
    }
    menusHTML += `</p>`;

    // origin ----------------------------------------
    if (data[i].origin) {
      menusHTML += `<p class="herkunft"><i class="fas fa-map-marker-alt"></i>&nbsp;${data[i].origin}</p>`;
    } else if(data[i].vegetarian == true) {
      menusHTML += `
      <p class="herkunft vegi">
        <i class="fas fa-seedling"></i>&nbsp;
        Vegetarisch
      </p>
      
      <img onclick="M.toast({html: 'Dieses Menu beinhaltet weder Fleisch noch Fisch.',classes:'green'})" src="/img/vegi.png" style="cursor:pointer;position:absolute;width:50px;bottom:0;right:0;padding:0 12px 12px 0;">
      `;
    }

    // more info below ----------------------------------------
    if(data[i].allergens || data[i].nutrients.length > 0){
      menusHTML += `<div id="info-${i}" style="display:none" class="info">`;
    }

    // allergens ----------------------------------------
    if (data[i].allergens) {
      menusHTML += `<p style="font-weight: bold;padding-top:5px">Allergene</p>`;
      menusHTML += `<ul class="allergene browser-default" style="margin:0">`;
      // for each allergen
      for (let j = 0; j < data[i].allergens.length; j++) {
        // get allergen
        const allergen = data[i].allergens[j];
        // add allergen to menu
        menusHTML += `<li>${allergen}</li>`;
      }
      menusHTML += `</ul><br>`;
    }

    // nutrients ----------------------------------------
    if (data[i].nutrients.length > 0) {
      menusHTML += `<table class="nutrients"><tbody>`;
      menusHTML += `<tr><th>Nährwerte</th><th>pro Portion (ca.)</th></tr>`;
      for (let j = 0; j < data[i].nutrients.length; j++) {
        let nutrient = data[i].nutrients[j];
        switch (nutrient.name) {
          case "kcal":
            nutrient.name = "Energie";
            nutrient.value = nutrient.value + " kcal";
            break;
          case "Kohlen-hydrate":
            nutrient.name = "Kohlenhydrate";
            nutrient.value = parseInt(nutrient.value) + " g";
            break;
          case "Fett":
            nutrient.name = "Fett";
            nutrient.value = parseInt(nutrient.value) + " g";
            break;
          case "Eiweiss":
            nutrient.name = "Eiweiss";
            nutrient.value = parseInt(nutrient.value) + " g";
            break;
        }
        menusHTML += `<tr><td>${nutrient.name}</td><td>${nutrient.value}</td></tr>`;
      }
      menusHTML += `</tbody></table><br>`;
    }

    if(data[i].allergens || data[i].nutrients.length > 0){
      menusHTML += `</div>`;
    }

    // add closing tags
    menusHTML += `</div></div></div>`;
  }
  // add menu to output
  output.innerHTML = menusHTML;
}

function showInfo(id, thisButton){
  // if the button is down, change it to up, vice versa
  if(thisButton.innerHTML.includes("down")){
    thisButton.innerHTML = thisButton.innerHTML.replace("down", "up");
  } else {
    thisButton.innerHTML = thisButton.innerHTML.replace("up", "down");
  }

  // get the info div
  const info = document.getElementById(id);

  // if the info div is hidden, show it, vice versa
  if(info.style.display == "none"){
    info.style.display = "block";
    info.style.height = "auto";
  } else {
    info.style.height = "0px";
    info.style.display = "none";
  }
}