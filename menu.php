<?php
// Get the menu from the API ---------------------------------------------------------------
$date = date('d.m.');
$api_base_url = 'https://api-prod.fivemoods.ch/menu';
$apiUrl = $api_base_url . '/date/' . $date;

if (isset($_GET['date'])) {
    $date = $_GET['date'];
    $apiUrl = $api_base_url . '/date/' . $date;
}

if (isset($_GET['weekday'])) {
    $weekday = strtolower($_GET['weekday']);
    $apiUrl = $api_base_url . '/day/' . $weekday;
    $date = date('d.m.', strtotime($weekday));
}

// Make API request
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $apiUrl);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$output = curl_exec($ch);
curl_close($ch);

// Decode the JSON response
$menu = json_decode($output, true);

// Create a new image -----------------------------------------------------------------------
$imageWidth = 650;
$imageHeight = 380;
$im = imagecreatetruecolor($imageWidth, $imageHeight);
// colors
$black = imagecolorallocate($im, 0, 0, 0);
$white = imagecolorallocate($im, 255, 255, 255);
$blue = imagecolorallocate($im, 3, 155, 229);
// Vegetarian Icon
$vegiIcon = imagecreatefrompng('./img/vegi.png');
$vegiIcon = imagescale($vegiIcon, 30, 30);
// logo
$logo = imagecreatefrompng('./img/mstile-144x144.png');
$logo = imagescale($logo, 50, 50);

// Make the background white
imagefilledrectangle($im, 0, 0, $imageWidth, $imageHeight, $white);

// Title ------------------------------------------------------------------------------------
$dateText;

// set $dateText to 'Heute' if $date is today, else set it to the Weekday
if ($date == date('d.m.')) {
    $dateText = 'Heute';
} else {
    $dateText = DateTime::createFromFormat('d.m.', $date);
    $dateText = $dateText->format('l');

    switch($dateText){
        case 'Monday':
            $dateText = 'Montag';
            break;
        case 'Tuesday':
            $dateText = 'Dienstag';
            break;
        case 'Wednesday':
            $dateText = 'Mittwoch';
            break;
        case 'Thursday':
            $dateText = 'Donnerstag';
            break;
        case 'Friday':
            $dateText = 'Freitag';
            break;
        case 'Saturday':
            $dateText = 'Samstag';
            break;
        case 'Sunday':
            $dateText = 'Sonntag';
            break;
    }
}

$dateFontSize = 30;
$dateAngle = 0;
$dateX = 20;
$dateY = 50;
if (empty($menu)) {
    // If menu is empty, show 'Heute kein Menu'
    $dateText .= ' kein Menu';
} else {
    $dateText .= ' im Five Moods';
}
imagettftext($im, $dateFontSize, $dateAngle, $dateX, $dateY, $black, './img/roboto-bold.ttf', $dateText);

// Draw a line below the title --------------------------------------------------------------
$lineY = 70;
$lineX1 = 20;
$lineX2 = $imageWidth - 20;
imageline($im, $lineX1, $lineY, $lineX2, 70, $black);

// Menu Items --------------------------------------------------------------------------------
$menuTitleTextSize = 20;
$descriptionTextSize = 10;
$textY = 110;
$textAngle = 0;
$textX = 20;
foreach ($menu as $item) {
    // title + price
    $menu_title = $item['title'] . ' (' . $item['siemensprice'] . ' ' . $item['currency'] . ')';
    imagettftext($im, $menuTitleTextSize, $textAngle, $textX, $textY, $black, './img/roboto.ttf', $menu_title);
    // if item is vegetarian, add the vegetarian icon
    if ($item['vegetarian'] == true) {
        // add image to the right of the title
        $imageX = $textX + imagettfbbox($menuTitleTextSize, $textAngle, './img/roboto.ttf', $menu_title)[2] + 10;
        imagecopy($im, $vegiIcon, $imageX, $textY - 25, 0, 0, 30, 30);
    }
    // description
    imagettftext($im, $descriptionTextSize, $textAngle, $textX, $textY + 15, $black, './img/roboto.ttf', $item['description']);
    // increase textY for the next item
    $textY += 50;
}

// Logo --------------------------------------------------------------------------------------
imagecopy($im, $logo, $imageWidth - 70, 10, 0, 0, 50, 50);

// date under logo ----------------------------------------------------------------------------
$dateString = DateTime::createFromFormat('d.m.', $date);
$dateString = $dateString->format('d.m.y');
// imagettftext($im, 10, 0, $imageWidth - 67, 69, $black, './img/roboto.ttf', $dateString);
imagettftext($im, 15, 0, 555, 370, $black, './img/roboto.ttf', $dateString);

// Footer ------------------------------------------------------------------------------------
imageline($im, 20, 350, $imageWidth - 20, 350, $black);
imagettftext($im, 15, 0, 20, 370, $black, './img/roboto-italic.ttf', 'Entdecke das ganze Menu auf');
imagettftext($im, 15, 0, 290, 370, $blue, './img/roboto-italic.ttf', 'fivemoods.ch');

// Vegetarian Icon legend --------------------------------------------------------------------
// $vegiIcon = imagescale($vegiIcon, 25, 25);
// imagecopy($im, $vegiIcon, 520, 352.5, 0, 0, 25, 25);
// imagettftext($im, 10, 0, 550, 370, $black, './img/roboto.ttf', '= Vegetarisch');

// Output the image
header('Content-type: image/png');
imagepng($im);
?>
